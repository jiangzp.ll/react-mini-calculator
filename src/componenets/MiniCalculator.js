import React, {Component} from 'react';
import './miniCalculator.less';

class MiniCalculator extends Component {
  // render() {
  //   return (
  //     <section>
  //       <div>计算结果为:
  //         <span className="result"> 0 </span>
  //       </div>
  //       <div className="operations">
  //         <button>加1</button>
  //         <button>减1</button>
  //         <button>乘以2</button>
  //         <button>除以2</button>
  //       </div>
  //     </section>
  //   );
  // }

    constructor(props, context) {
        super(props, context);
        this.state = {result: 0};
        this.plus = this.plus.bind(this);
        this.multiplyTwo = this.multiplyTwo.bind(this);
        this.divisionTwo = this.divisionTwo.bind(this);
        this.subtraction = this.subtraction.bind(this);
    }


    render() {
        return (
            <section>
                <div>计算结果为:
                    <span className="result"> {this.state.result} </span>
                </div>
                <div className="operations">
                    <button onClick={this.plus}>加1</button>
                    <button onClick={this.subtraction}>减1</button>
                    <button onClick={this.multiplyTwo}>乘以2</button>
                    <button onClick={this.divisionTwo}>除以2</button>
                </div>
            </section>
        );
    }

    plus() {
        this.setState({result: this.state.result + 1});
    }

    subtraction() {
        this.setState({result: this.state.result - 1});
    }

    multiplyTwo() {
        this.setState({result: this.state.result * 2});
    }

    divisionTwo() {
        this.setState({result: this.state.result / 2});
    }

}

export default MiniCalculator;

